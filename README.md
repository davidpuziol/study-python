# study-Python

<img src="./pics/python.png" alt="drawing" width="200"/>

Esse é um estudo inicial da linguagem Python. Hoje é linguagem mais utilizada do mercado. Costuma ser a porta de entrada para quem esta buscando a primeira linguagem de programação. É a mais utilizada pelo DevOps.

<https://www.Python.org/>

- Linguagem interpretada com comando simples e fácil de entender
  - Diferente de uma linguagem compilada que precisar sofrer o processo de compilação, o Python é uma linguagem scriptada (interpretada), ou seja, a compilação do código é feito em tempo de execução.
- Lançada no início da decada de 90.
- Em 2001, com a linguagem já com uma comunidade forte, criaram a Python Software Foundation (PSF), que é uma das mantenedoras e coordenados do Python. É uma fundação sem fins lucrativos apoiada por vários grupos como Microsoft, Google, etc.
- Já vem pré instalado em muitos sistemas operacionais (todas as distro linux, mac, etc), menos no Windows.
- Muitos projetos de computação e Internet das coisas utiliza o Python como linguagem padrão. Por exemplo Raspeberry Pi tem como um dos seus propósitos ser um ambiente para aprendizado da linguagem Python.

## Por que aprender Python?

- É uma linguagem de propósito geral.
- Simples, fácil, intuitivo e Organizada.
- Multiplatafoma.
- Orientada a objeto.
- Batteries included (A maioria dos recusos já é built in).
- Open source permissive license.
- Muitas bibliotecas disponíveis.
- Comunidade muito presente.
  
Zen do Python

```bash
Bonito é melhor que feio.
Explícito é melhor que implícito.
Simples é melhor que complexo.
Complexo é melhor que complicado.
Linear é melhor do que aninhado.
Esparso é melhor que denso.
Legibilidade conta.
Casos especiais não são especiais o bastante para quebrar as regras.
Ainda que praticidade vença a pureza.
Erros nunca devem passar silenciosamente.
A menos que sejam explicitamente silenciados.
Diante da ambiguidade, recuse a tentação de adivinhar.
Dever haver um — e preferencialmente apenas um — modo óbvio para fazer algo.
Embora esse modo possa não ser óbvio a princípio a menos que você seja holandês.
Agora é melhor que nunca.
Apesar de que nunca normalmente é melhor do que *exatamente* agora
Se a implementação é difícil de explicar, é uma má ideia
Se a implementação é fácil de explicar, pode ser uma boa ideia
Namespaces são uma grande ideia — vamos ter mais dessas!
```

## Instalando o Python

A chance dele já estar intalado é muito grande.
Básicamente o Python é um interpretador do código para linguagem que a máquina entende.

Se estive no windows, baixe e instale o Python em <https://www.Python.org/downloads/>. Em outros sistema operacionais já vem instalado.

Atualize o Python

```bash
# Base debian
sudo apt install Python
# Base Red Hat
sudo yum install Python
# Base Arch
sudo pacman -S Python
```

Para verificar a versão

```bash
python --version
```

## Como estudar?

Siga as documentações abaixo sem pular nenhuma etapa. A pasta [manuscritos](./manuscritos/) possui um resumo do estudo de Python que acho importante aprender, mas não quer dizer que tudo estará lá. **Eu já sei programar** e algumas coisas provavelmente eu esquecerei de explicar, pois para mim pode parecer óbvio, mas talvez não para você que estiver começando. Tentarei melhorar ao longo do tempo.

Não deixe de acompanhar com a [documentação oficial](https://docs.Python.org/3/tutorial/index.html).

- O estudo não é suficiente se você não praticar. Ver alguém tocar piano não quer dizer que você tocará só vendo.
- Se você copiar e colar os exemplos você será um ótimo copiador não programador e nunca terá novas ideias.

[Apresnetação](./manuscritos//0-apresentando.md)

[Variáveis Primitivas](./manuscritos/1-variaveis-primitivas.md)
[Variáveis de Coleções](./manuscritos//1.1-colecoes.md)

[Operadores Aritméticos](./manuscritos/2-operadores-aritmeticas.md)
[Operadores Lógicos](./manuscritos/2.1-operadores-logicos.md)

[Bibliotecas(Módulos)](./manuscritos/3-modulos.md)
[Módulos Importantes](./manuscritos/3-modulos.md)

[Estruturas de Controle](./manuscritos/4-controle.md)

[strings e listas](./manuscritos/strings%20e%20listas.md)

# Estrutura de controle

Um algorítmo é uma receita, seu código fonte, um processo é um algorítmo em execução.
Durante o processamento do algorítmo muitos caminhos (desvios) precisam acontecer.

Um exemplo gráfico de um algorítmo bem maluco!

![Solucionador](../pics/solucionador.png)

Isso é um claro exemplo de que vários caminhos podem ser tomados.

Então vamos conhecer algumas condicionais. Estas condicionais são as mesmas para todas as linguagem, o que muda é como declarar.

## Blocos de código

Uma grande parte das linguagens utiliza chaves `{ }` para delimitar os blocos e ao final de cada linha um ; ou algum outro delimitador para avisar que a linha acabou ficando mais ou menos assim

```c
statement (condicao) {
    primeira linha do bloco;
    segunda linha do bloco;
    terceira linha (condicao) {
        primeira linha do sub bloco;
    }
}
```

Mas o criador do python preferiu simplificar usando somente a identação sem os {} e sem os delimitadores.

```python
statement (condicao):
    primeira linha do bloco
    segunda linha do bloco
    terceira linha (condicao):
        primeira linha do sub bloco
```

Sobre a identação que o ideal é separar com um `tab` ou 4 espaços que é a convenção padrão utilizada. A maior parte dos editores de código vão ajudar nisso.

## IF (Se)

Se uma condição for verdadeira execute o bloco abaixo senão execute o outro.

>É o condicional mais usado em qualquer linguagem

Vamos fazer o programa acima com a primeira pergunta...  A bagaça funciona? `Se for sim, será true, se for não será false`.  Um if somente executa o bloco a baixo se uma condição for verdade e isso acontece quando algum retorno é booleano, true ou false.

```python
# Vai esperar True ou false
funciona = bool(input("A bagaça funciona? "))
if funciona == True:
  print("NEM RELA")
else: # Vai continuar o bloco do False (não)
  ... vai continuar o programa daqui
```

Se funciona é True entao vai printar NEM RELA. Senão continua o programa.

Precisamos entender antes de continuar como funciona os comparadores.

- `==` Irá comparar se uma é igual ao outro. É diferente do `=`que atribui o valor para dentro de uma variável.
- `!=` Irá comparar se é diferente.
- `>` Maior
- `>=` Maior ou igual
- `<` Menor
- `<=` Menor ou igual
- `in` Esta dentro. Usamos mais para strings e listas
- `is` Usado para conferir se é ou não é. Usado em strings.

Veja o exemplo e observe a identação (alinhamento) do bloco abaixo do if e do else. O python precisa que essa identação seja correta com um tab por exemplo. Ao final do if e do else precisamos ter o `:`. Para o python é o aviso que a expressão que irá formar a condição acabou.

```python
idade = 18

if idade >= 18:
### bloco ####   Veja que tem está um TAB a frente tudo que estará dentro desse bloco 
    print("Você é maior de idade.")
    print("Bem-vindo(a)!")
else:
### bloco ####
    print("Você é menor de idade.")
    print("Desculpe, acesso restrito.")
```

Agora vamos imaginar no mesmo exemplo acima, se a idade for menor que 10, manda ir dormir.

```python
if idade >= 18:
### bloco ####
    print("Você é maior de idade.")
    print("Bem-vindo(a)!")
else:
### bloco ####
    if idade < 10:
    ### bloco ####
        print("Vai dormir.")
    else:
    ### bloco ####
        print("Você é menor de idade.")
        print("Desculpe, acesso restrito.")

print("Continuando...")
```

Existe também a estrutura condicional aninhada. É a ideia de se, senao se, senao se, senao se, senao se, senao, de forma que a primeira condição que satisfaça será executada e o resto pulada.

```python
idade = 25

if idade < 18:
    print("Você é menor de idade.")
elif idade >= 18 and idade < 21:
    print("Você é maior de idade, mas ainda não pode beber nos Estados Unidos.")
elif idade >= 21 and idade < 65:
    print("Você é maior de idade e pode beber nos Estados Unidos.")
elif idade >= 65:
    print("Você é um idoso.")
```

## While (enquanto)

Estrutura de repetições chamamos de laço ou loop

A estrutura de controle while cria um loop na execução do bloco enquanto a condição é verdadeira.  No nosso algorítmo gráfico a pergunta `Dá para jogar a culpa em alguém?` cria um uma repetição enquanto ele reponder `Não`

Dessa vez vamos criar uma condição por exemplo comparando uma string. Enquanto ele não for "sim" Não vai sair do loop.

```python
# Definindo o valor de entrada
condicao = "nao"
while condicao != "sim":
    ### Bloco
    print("SE FODEU")
    condicao = input("Dá para jogar a culpa em alguém? ")
### Fora do bloco, vai executar assim que o while sair
print("SEM PROBLEMAS")
```

Veja o exemplo abaixo. Irá imprimir uma contagem de 0 a té 4, pois 5 já não iria satisfazer a condição.

```python
contador = 0
while contador < 5:
    print("Contagem:", contador)
    # somando 1 ao contador
    contador += 1
    # mesma coisa que contador = contador + 1
print("Fim do loop!")
```

Se você precisar entrar no bloco do while para executar pelo menos uma vez, do tipo primeiro faça e depois pergunte, é possivel definir um bloco while como `True` ou seja um loop infinito e depois verificar a condição dentro do bloco utilizando o `if`. A palavra mágica que irá parar a execução é o `break`.

o `elif` é uma opção do tipo `senão se`. Podem ter quantos elif quiser, mas para esse cenário com muitas opções temos outras estruturas de controle. Não existe uma regra, mas as boas práticas dizem que no máximo 2 elif é o sufidiente, acima disso já iremos partir para uma estrutura chamada `case` que veremos depois.

No exemplo abaixo podemos verificar..

- se for s pare.
- senão, se, for c continue.
- senão, tente novamente

Veja o exemplo:

```python
resposta = ""
while True:
    resposta = input("Digite 's' para sair ou 'c' para continuar: ")
    if resposta == 's':
        break
    elif resposta == 'c':
        print("Continuando...")
    else:
        print("Opção inválida. Tente novamente.")

print("Fim do loop!")
```

## Exercício 1

Implemente toda a lógica do nosso algorítmo gráfico. Nesse exercício só precisa dos controle if else e while. O código esta em [solucionador.py](../examples/4-controle/solucionador.py).

## Exercício 2

Escreve um programa em python para aprovar um emprestimos para a compra de uma casa.

Pergunte:

- O valor da casa
- A renda do comprador
- Quantos anos ele quer pagar a casa

Saída do programa:

- A prestação mensal
- Se ele pode ou não comprar essa casa sendo que a parcela precisa ser menor ou igual 30% do salário do comprador.

O código esta em [compra_casa.py](../examples/4-controle/compra_casa.py).

## Exercício 3

Escreve um programa que leia um numero inteiro e pergunte ao usuário para entrar com 1 2 ou 3.
Caso digite:

1. Converta para binario
2. Converte para octal
3. Converte para hexadecimal

O código esta em [converte.py](../examples/4-controle/converte.py).

## Exercício 4

Escreva um programa que entre nois numeros e compare quem é o maior ou se são iguais

O código esta em [compare.py](../examples/4-controle/compare.py).

## Exercício 5

Leia a data de nascimento de um aluno de natação e diga qual a categoria ele pertence

- até 9 anos é mirim
- até 14 é infantil
- até 19 é junior
- até 25 é senior
- acima de 25 é master

O código esta em [categoria.py](../examples/4-controle/categoria.py).

um outro código [categoriav2.py](../examples/4-controle/categoriav2.py leva em consideração até o dia do nascimento para fazer o calculo.

## Exercício 6

Crie um programa que jogue pedra papel tesoura com voce.

O código esta em [jokenpo.py](../examples/4-controle/jokenpo.py).

Estrutura

## For (até que)

A estrutura de laço `for` diferente da regra while ela não necessita que algo seja verdadeiro para executar, mas de um número de execução pré definido.

A estrutura de laço for em Python é usada para iterar sobre uma sequência de elementos, como uma lista, uma string, um range, entre outros. Ela executa um bloco de código repetidamente para cada elemento da sequência.

```python
for elemento in sequencia:
    # código a ser executado
```

Vamos analisar cada parte dessa estrutura:

- A palavra-chave `for` inicia a declaração do laço for.
- `elemento` é uma variável (poderia ser qualquer outra) que receberá o valor de cada elemento da sequência em cada iteração.
- A palavra-chave in separa a variável da sequência em que será iterado.
- sequencia é uma sequência de elementos sobre a qual o laço for irá iterar. Pode ser uma lista, uma string, um range, uma tupla, entre outros.

Irá ser feito para todos itens da lista ou até encontrar um break.

```python
nomes = ["David", "Maria", "Pedro", "João"]
for nome in nomes:
     print(nome)
... 
David
Maria
Pedro
João
```

Por exemplo com o break

```python
nomes = ["David", "Maria", "Pedro", "João"]
for nome in nomes:
     if nome == "Pedro":
       break
     else:
        print(nome)
...
David
Maria
....
```

Se vc quiser garantir a iteração de uma lista por algumas vezes pré definidas irá utilizar o `range`.

```python
# o range de 0 a 10 é o que?
print(list(range(0,10)))
for c in range(0,10):
    print(f"c vale {c}")
...
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
c vale 0
c vale 1
c vale 2
c vale 3
c vale 4
c vale 5
c vale 6
c vale 7
c vale 8
c vale 9
....
```

Se quiser imprimir o contrário?

```python
print(list(range(10,0,-1)))
for c in range(10,0,-1):
    print(f"c vale {c}")
...
[10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
c vale 10
c vale 9
c vale 8
c vale 7
c vale 6
c vale 5
c vale 4
c vale 3
c vale 2
c vale 1
...
```

Se quiser pular de 2 em 2 ainda..

```python
print(list(range(10,0,-2)))
for c in range(10,0,-2):
    print(f"c vale {c}")
[10, 8, 6, 4, 2]
c vale 10
c vale 8
c vale 6
c vale 4
c vale 2
```

Observe uma coisa, você precisa entregar para o for a lista que vc quer que ele faça a iteração, então teste sempre a lista, pois se você reparar a lista invertida que mostrei não tem os mesmo valores da lista ordenada. Uma foi de 0 a 9 e a outra de 0 a 10. Tenha cuidado e faça os testes necessários sempre.

## Exercicio 7

Faça uma contagem regressiva de 10 segundos.
O código esta em [regressiva.py](../examples/4-controle/regressiva.py).

## Exercicio 8

Imprima somente os numero pares e depois somente os ímpares a partir de um número inicial dado pelo usuário até outro número final.

O código esta em [par_impar.py](../examples/4-controle/par_impar.py).

Esse é um exercício muito importate sobre desempenho. A solução acima funciona, mas exige mais do processador do que a solução abaixo.

O código esta em [par_imparv2.py](../examples/4-controle/par_imparv2.py).

A diferente é que a primeira tentativa executa muito mais ifs do que a segunda ocupando o seu processador. Se você executar um if dentro de um laço irá executar TODAS as vezes. No segundo exemplo já foi dado ao for a lista pronta para ele fazer o laço.

Outro detalhe é que a primeira lista que não tem range é executada inteira duas vezes, enquanto a segunda tem só a metade da lista.

>Essa é a diferença entre um iniciante e um expert. Esse tipo de detalhe que faz um programa ser "leve" e rodar em máquinas com menos poder de processamento. Além da experiência com programação o amadurecimento matemático do programador faz diferença nesse tipo de coisa.

## Exercicio 9

Some todos os valores que são divisiveis por 3 e são impares de zero até a entrada que o usuário escolher.

Se liga no desempenho!

O código esta em [div3_impar.py](../examples/4-controle/div3.py).

## Exercicio 10

Confira se um número é primo

Se liga no desempenho!

O código esta em [div3_impar.py](../examples/4-controle/div3.py).

## Exercicio 11

Esse exercicio vamos usar listas e tuplas. Imprimir a lista de crianças agrupadas por sala
que frequentas cada uma das atividades.

Dados:

sala1 = ["Erik", "Maia", "Gustavo", "Manuel", "Sofia", "Joana"]
sala2 = ["Joao", "Antonio", "Carlos", "Maria", "Isolda"]

aula_ingles = ["Erik", "Maia", "Joana", "Carlos", "Antonio"]
aula_musica = ["Erik", "Carlos", "Maria"]
aula_danca = ["Gustavo", "Sofia", "Joana", "Antonio"]

O código esta em [div3_impar.py](../examples/4-controle/relatorio.py).

Agora faça o mesmo exercicio usando set e dict

O código esta em [div3_impar.py](../examples/4-controle/relatorio_set.py).
O código esta em [div3_impar.py](../examples/4-controle/relatorio_dict.py).

# Operadores aritméticos

Com relação aos operadores que podemos fazer no python temos o seguinte.

- Adição
  - 4+3 = 7
- Subtração
  - 4-3 = 1
- Multiplicação
  - 4*3 = 12
- Divisão
  - 4/3 = 1.3333333
- Potenciação
  - 4**3 =64
- Divisão inteira
  - 4//3 =1
- Resto da divisão
  - 4%3 =1

A ordem da preferência é

1. O que está entre `()`
2. `**`
3. `*`, `/`, `//`, `%` quem aparecer primeiro
4. `+`, `-` quem aparecer primeiro

Abrindo o console vamos fazer uns testes

```python
>>> 5+4**2
21
>>> 18//2*(5+2)*2
126
>>> 18//2*(5+2)*2+4
130
>>> 567**333
87732117446070003715053516180852867561733510490298952637287497431563317374084143995465490560216107876808702873487868667862271552903064662596725150619327746079148174413688662023190539128122361705889382443530458156182022076736458993036481642794482475236289311330701329291855996885404789485786551483458396449973434920640205792038734126774527885373180925993193311014948558974951069075316968365528706238117357359458909312301720409726208656326771195846753582287854877372147618520442083048255420664761304358432564719724043959812977568801553053999616645045035274573820396436155101285704381990481895392992363684440625177127180036028294946917143399860454006028454837777640777962785150665504496193057912004099984540159030535471388553223000248429007559122198684670105604831537145651461714172578435020765490665307159386361128416692210916174057769498026339495380358225940846121826164215336097323274422272026998129778089404958995287
>>> 347.04**33.7
4.081222933285245e+85
>>> 49**(1/2) # isso é uma raiz quadrada
7.0
>>> 49**(1/3) # raiz cúbica
3.6593057100229713
>>> pow(5,2) # Funçao interna do python que calcula a potencia
25
```

No Python o limite de um número é o tamanho da memória e por isso ele é reconhecido na parte de cáculos científicos.

Existem outras maneiras de fazer calculos matemáticos no python usando a biblioteca math, mas veremos mais pra frente.

## Exercicio 1

Leia um número inteiro e mostre seu antecessor e sucessor.
A resolução do está em [antecessor_sucessor.py](../examples/2-operadores-aritimeticos/antecessor_sucessor.py)

## Exercicio 2

Leia um número e mostre seu dobro triplo e raiz quadrada.
A resolução do está em [dobro_triplo_raiz.py](../examples/2-operadores-aritimeticos/dobro_triplo_raiz.py)

## Exercicio 3

Leia duas notas de um aluno e calcule a sua média. Atenção na ordem de precedência
A resolução do está em [media.py](../examples/2-operadores-aritimeticos/media.py)

## Exercicio 4

Leia um valor em metros e conversa em centimestros e milimetros. Quero poder ler 2.3 metros por exemplo.
A resolução do está em [converte.py](../examples/2-operadores-aritimeticos/converte.py)

## Exercicio 5

Leia um número inteiro e imprima sua tabuada da melhor maneira que conseguir.
>Dica: `/n` pula de linha
A resolução do está em [tabuada.py](../examples/2-operadores-aritimeticos/tabuada.py)

## Exercicio 6

Leia o valor para quanto custa 1 dolar no momento e outro valor em real que você têm e veja quandos dolares consegue comprar.
Como o primeiro desafio e sair da sua zona de conforto, se conseguir, tente usar somente duas casas decimais. Procure na internet a solução antes de ver o resultado.
A resolução do está em [dollar.py](../examples/2-operadores-aritimeticos/dollar.py)

## Exercicio 7

Sabendo um 1 litro de tinta pinta 2 metros quadrados, leia a altura e largura de uma parede e me diga quantos litros de tinta irei precisar.
A resolução do está em [tinta.py](../examples/2-operadores-aritimeticos/tinta.py)

## Exercicio 8

Leia o preço de um produto e mostre o seu valor com 8% de desconto.
A resolução do está em [desconto.py](../examples/2-operadores-aritimeticos/desconto.py)

## Exercicio 9

Leia o salário de uma pessoa e mostre ele com 17% de aumento.
A resolução do está em [salario.py](../examples/2-operadores-aritimeticos/salario.py)

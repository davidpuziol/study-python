# Coleções

## Tuplas (tuple)

Representam coleções ordenadas e imutáveis de elementos. Usam `()` entre seus valores. Uma vez criada a tupla, não é possivel alterar, não dá para mudar os valores ou adicionar novos.

As tuplas são o tipo composto mais simples de todos e bastante comum de serem usadam em Python, da mesma forma que anteriormente vimos que a string "ABC" é uma sequência de carecteres, com as tuplas conseguimos fazer uma sequência de valores que podem ser de qualquer tipo.

Em Python sempre que um ou mais objetos forem encadeados com `,` isso será interpretado como um objeto do tipo `tupla`.

A caracteristica mais interessante das tuplas se chama **unpacking** ou desempacotamento. O desempacotamento permite fazer a operação contrária da atribuição.

```python
# Atribuição, poderia não ter parênteses
>>> pessoa = ("David", 37, "Vila Velha", "david@example.com")
# Desempacotamento
>>> nome, idade, cidade, email = pessoa
>>> print("Nome:", nome)
Nome: David
>>> print("Idade:", idade)
Idade: 37
>>> print("Cidade:", cidade)
Cidade: Vila Velha
>>> print("Email:", email)
Email: david@example.com
>>> pessoa[0]
'David'
>>> pessoa[1]
37
>>> 
```

## Listas (list)

Representam coleções ordenadas e mutáveis de elementos. Usam `[]` entre seus valores. Listas são bastante similares as tuplas e a maioria das operações que podemos fazer com tuplas também podemos fazer com as listas, a diferença é que podemos adicionar, remover e reordenar elementos, ou seja, são mutáveis.

```python
>>> frutas = ["maçã", "banana", "laranja", "morango"]
>>> print(frutas)
['maçã', 'banana', 'laranja', 'morango']
>>> type(frutas)
<class 'list'>
>>> print(frutas[0])
maçã
>>> print(frutas[1])
banana
# Adicionar amora ao final
>>> frutas.append("amora")
# Inserir abacaxi na posicao 0
>>> frutas.insert(0, "abacaxi")
# Inserir abacate na posicao 3
>>> frutas.insert(3, "abacate")
>>> print(frutas)
['abacaxi', 'maçã', 'banana', 'abacate', 'laranja', 'morango', 'amora']
>>> 
# Somando listas
>>> frutas2 = ["maracuja","uva"]
>>> nova_frutas = frutas + frutas2
>>> print(nova_frutas)
['abacaxi', 'maçã', 'banana', 'abacate', 'laranja', 'morango', 'amora', 'maracuja', 'uva']
# Removendo banana da nova lista
>>> nova_frutas.remove("banana")
>>> print(nova_frutas)
['abacaxi', 'maçã', 'abacate', 'laranja', 'morango', 'amora', 'maracuja', 'uva']
# Removendo o ultimo elemento
>>> nova_frutas.pop()
'uva'
>>> print(nova_frutas)
['abacaxi', 'maçã', 'abacate', 'laranja', 'morango', 'amora', 'maracuja']
>>> 
```

## Conjuntos (set)

Representam coleções não ordenadas e não duplicadas de elementos. Por padrão, eles não possuem uma ordem específica, o que significa que não há garantia de qual elemento será considerado o "primeiro" elemento. Dessa forma geralmente transforma-se os sets alguma lista ordenada para impressão. Existe um contjunto chamado fronzenset (fset) que não permite adicionar e remover.

```python
>>> cores
{'azul', 'vermelho', 'verde'}
>>> cores = {"vermelho", "verde", "azul"}
>>> cores
{'azul', 'vermelho', 'verde'}
>>> type(cores)
<class 'set'>
>>># Vou forçar a entrada de dois vermelhores que não deveria ser repetido.
>>> cores = {"vermelho", "verde", "azul", "vermelho"}
>>> type(cores)
<class 'set'>
>>> cores
{'azul', 'vermelho', 'verde'} #Mesmo assim ele eliminou o vermelho extra.
>>># Adicionando e removendo cores
>>> cores.add("amarelo")
>>> cores.remove("verde")
>>> print("azul" in cores)
>>># Observe a função print aqui verifica se temos as chaves e não mostra o seu valor. 
True
>>> print("preto" in cores)  # Saída: False
False
>>> print(cores)
{'azul', 'vermelho', 'amarelo'}
>>># Precisamos de outros recursos para imprimir um set, que serão vistos mais para frente. Não se importe com isso agora.
>>> for cor in cores:
...     print(cor)
... 
azul
vermelho
amarelo
```

## Dicionários (dict)

Representam coleções de pares chave-valor, onde cada valor é acessado através de sua chave. Os dicionários são um misto entre o `set` e `list`.

```python
>>> pessoa = {
...     "nome": "David",
...     "idade": 36,
...     "cidade": "Vila Velha",
...     "email": "david@example.com"
... }
>>> print("Nome:", pessoa["nome"])
Nome: David
>>> print("Idade:", pessoa["idade"])
Idade: 36
>>> print("Cidade:", pessoa["cidade"])
Cidade: Vila Velha
>>> print("Email:", pessoa["email"])
Email: david@example.com
>>># Adicionando um novo par chave-valor ao dicionário
>>> pessoa["telefone"] = "(11) 01234-5678"
>>># Modificando um valor existente no dicionário
>>> pessoa["idade"] = 37
>>># Removendo um par chave-valor do dicionário
>>> del pessoa["email"]
>>> print(pessoa)
{'nome': 'David', 'idade': 37, 'cidade': 'Vila Velha', 'telefone': '(11) 01234-5678'}
>>> 
```

Também podemos ter objetos compostos

```py
produto = {
    "nome": "Caneta",
    "cores": ["azul", "branco"],
    "preco": 3.23,
    "dimensao" {
        "altura": 12.1,
        "largura":  0.8,
    },
    "em_estoque: True,
    "codigo": 45678,
    "codebar": None,
}

# E para iniciar vazio
cliente = {}
# ou
cliente = dict()
```

Também é possível combinar dois dicionários.

```py

>>> cliente = {"nome": "David", "cidade": "Vila Velha"}
>>> extra = {"cpf": "123456789-00"}
>>> final = {**cliente, **extra}
>>> print(final)
{'nome': 'David', 'cidade': 'Vila Velha', 'cpf': '123456789-00'}
```

Ou fazendo `update` in place.

```py
cliente.update({"estado": "es"})
>>> print(cliente)
{'nome': 'David', 'cidade': 'Vila Velha', 'estado': 'es'}
```

Caso uma chave não exista no dicionário o Python estoura um erro chamado `KeyError`

```py
print(cliente["telefone"])
...
KeyError 'telefone'
```

Para evitar o erro podemos usar o método `get` que busca a chave e caso não exista
retorna um valor padrão que inicialmente é `None`

```py
print(cliente.get("telefone"))
'None'
# Definindo o padrão de retorno para (00)99999-9999
print(cliente.get("telefone", "(00)99999-9999"))
'(00)99999-9999'
```

## Bytesarray (bytearray)

Representam sequências mutáveis de bytes que armazena uma sequência de bytes. É semelhante ao tipo de dado bytes, mas com a diferença fundamental de que um bytearray pode ser modificado, enquanto um objeto bytes é imutável. Isso torna essa estrutura adequada para situações em que você precisa alterar dados em nível de byte de forma eficiente.

```python
>>> dados = bytearray(b'RellO')
>>> dados[0] = 72  # Modificando o primeiro byte para 'H'
>>> dados[4] = 111  # Modificando o último byte para 'o'
>>> dados.append(33) # Adicionando o byte '!'
>>> print(dados)
bytearray(b'Hello!')
>>> dados.extend(b' World')  # Adicionando uma sequência de bytes
>>> print(dados)  # Saída: bytearray(b'Hello World!')
bytearray(b'Hello! World')
>>> lista_bytes = list(dados) # Convertendo um bytearray para uma lista de bytes
>>> print(lista_bytes)
[72, 101, 108, 108, 111, 33, 32, 87, 111, 114, 108, 100]
>>> 
```

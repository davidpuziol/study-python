# Variáveis

<https://docs.python.org/3/tutorial/introduction.html>

Sendo direto, vamos abrir um console python e começar a entender como o Python entende que uma coisa é uma string "texto" ou se é um número.

Faça vários testes nos console do Python para aprender.

>Propositalmente eu não criei nenhum script Python para que você não rote o script e veja a saída, mas execute os comandos dentro do console e tenha intimidade com o Python primeiro. Essa base de conhecimento deve ser fixada com muita prática.

- Tudo o que está em aspas duplas ou simples é um texto.
  - "Oi, eu sou um texto"

- Como na matemática `x + y = z`, x é uma varíavel assim como na liguagem, e uma variável pode ser qualquer coisa, desde um número com um valor, uma string (que representa um "texto"), um booleano que representa True ou False, ou até mesmo uma coleção de elementos. Para definir uma variável:
  - x = 5
  - y = 5.8
  - texto_teste = "Oi, eu sou um texto"
  - j = True

- Quando referenciamos uma varíavel já estamos pegando o valor dela, mas se quisermos printar (imprimir) para o console podemos usar função print() passando entre () o que devemos printar.

  - ```python
    >>> texto_teste = "Oi, eu sou um texto"
    >>> print(texto_teste)
    Oi, eu sou um texto
    >>>
    >>> print("7" + "4")
    74 <---- soma de strings, não faz o cálculo
    >>> print(7 + 4)
    >>> print("7" + 4)
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: can only concatenate str (not "int") to str
    # Deu erro por que ele esta tentando somar um string com um número
    >>> print(int("7") + 4)
    11 <---- a função int() transformou 7 string para 7 em inteiro para fazer a soma
    >>> x = 5
    >>> y = 4
    >>> print (x + y)
    9
    >>>
    ```

## Variável dinâmicamente tipadas

No Python, as variáveis são dinamicamente tipadas, o que significa que você não precisa declarar explicitamente o tipo de uma variável ao defini-la. Atribuir um valor a uma variável cria automaticamente a variável e determina seu tipo com base no valor atribuído. Além disso, o tipo de uma variável pode ser alterado durante a execução do programa, simplesmente atribuindo um valor de um tipo diferente à ela.

## Quais os tipos de variávels primitivas do Python?

Todas as informações que usaremos durante a programação são representadas na memória do computador através de um **tipo de dado**, você também vai ouvir os termos **classe** ou **categoria** para se referir a mesma coisa.

Todos os objetos em **Python** são formados por essas 3 propriedades:

- valor `"1000010" ou "B"`
- tipo ou classe `str, int, float, ...`
- posição na memória `o número retornado pela função id()`

Entenda o conceito e foque nesse momento em entender `int, float, e string`. Variáveis de coleções somente irão ser usadas posteriormente. A idéia de já chegar com esse conhecimento é abrir a cabeça sobre o que uma variável pode representar.

No Python nós não precisamos declarar qual o tipo de dado a ser usado pois o Python faz a inferência de tipos dinâmicamente, o interpretador primeiro verifica como o valor se parece e então decide por conta própria qual a classe a ser utilizada. Você até pode se desejar, forçar o tipo de dado explicitamente, mas isso é considerado redundante, não errado.

## Números inteiros (int)

> Essa parte da explicação será maior, pois tudo que estiver aqui vale para a maioria dos outros tipos.

Representam valores inteiros, como 1, 2, -3, etc.

```py
>>> idade = 25
>>> type(idade)
<class 'int'>
>>> quantidade = int(3) #Forca o tipo, mas é redundante
>>> type(quantidade)
<class 'int'>
```

Podemos fazer uma variedade de operações com este valor, essas operações fazem parte do que chamamos `Protocolo` do objeto, e quem define os protocolos que o objeto implementa é a classe `int`. Se fosse outra classe, como veremos mais adiante seriam outros protocolos.

>Cada classe tem diferentes protocolos, ou seja, funções que servem para manipulação o seu valor.

Para verificar tudo o que podemos fazer com um objeto do tipo int podemos fazer `dir(<tipo>)`.
Tudo que esta entre __ são os atributos especiais da classe e o que não tem é atributos publicos.

```python
>>> dir(int)
['__abs__', '__add__', '__and__', '__bool__', '__ceil__', '__class__', '__delattr__', '__dir__', '__divmod__', '__doc__', '__eq__', '__float__', '__floor__', '__floordiv__', '__format__', '__ge__', '__getattribute__', '__getnewargs__', '__getstate__', '__gt__', '__hash__', '__index__', '__init__', '__init_subclass__', '__int__', '__invert__', '__le__', '__lshift__', '__lt__', '__mod__', '__mul__', '__ne__', '__neg__', '__new__', '__or__', '__pos__', '__pow__', '__radd__', '__rand__', '__rdivmod__', '__reduce__', '__reduce_ex__', '__repr__', '__rfloordiv__', '__rlshift__', '__rmod__', '__rmul__', '__ror__', '__round__', '__rpow__', '__rrshift__', '__rshift__', '__rsub__', '__rtruediv__', '__rxor__', '__setattr__', '__sizeof__', '__str__', '__sub__', '__subclasshook__', '__truediv__', '__trunc__', '__xor__', 'as_integer_ratio', 'bit_count', 'bit_length', 'conjugate', 'denominator', 'from_bytes', 'imag', 'numerator', 'real', 'to_bytes']
```

A lista acima exibe os nomes de todos os atributos dos objetos armazenados com a classe `int`, tudo o que começa com `__` e termina com `__` nós chamamos de **métodos dunder** e eles são atributos especiais do modelo de dados do Python, nós não precisamos usar esses atributos diretamente em nosso código, porém é importante saber como isso é implementado pelo Python pois através de uma lista de atributos você será capaz de determinar o que aquele objeto suporta de operações. Nós iremos utilizar abstrações que por baixo dos panos irão fazer a chamada para esses métodos dunder. Por exemplo se você encontrar `__add_` significa que pode somar usando o `+`.

```bash
>>> preco = 10
>>> imposto = 2
>>> total = preco + imposto
>>> total2 = preco.__add__(imposto)
>>> print(total)
12
>>> print(total2)
12
>>> 
```

Neste momento nós não vamos falar de todos eles pois são muitos e a idéia é que aos poucos você vá entendendo conforme utiliza, mas vamos explorar um exemplo simples e que provavelmente usaremos sempre.

## Números de ponto flutuante (float)

Representam valores decimais, como 3.14, -0.5, etc. Se o número tiver sua parte decimal, ela deve ser separa por um `.` ele será um float por inferência.

```python
>>> valor = 5/2
>>> type(valor)
<class 'float'>
>>> print(valor)
2.5
```

## Caractere (chr)

Para armazenar um caractere usamos o chr. Uma string é um conjunto de caracteres, portanto a string "abc" é formada pelos caracteres `a b e c`.

Existem várias tabelas de caracteres usadas na computação mas nesse treinamento vamos ficar em apenas duas `ascii` e `utf8`. Cada caractere tem o seu código na tabela ascii. Não precisa decorar a tabela, visite-a quando precisar.

![ascii](../pics/ascii.png)

Já a tabela [unicode - utf8](https://symbl.cc/en/) todos os caracteres de todas as linguas incluindos emojis.

>>> chr(65)
'A'
>>> chr(66)
'B'
>>> chr(67)
'C'
>>> fruta = "🍉"
>>> fruta.encode("utf-8")
b'\xf0\x9f\x8d\x89'
>>>

## Booleanos (bool)

Representam valores lógicos Verdadeiro (True) ou Falso (False) sendo que a primeira letra é Maiusculo. Por padrão True vale 1 False vale 0.  Uma soma somará seus valores resultado em um novo valor, mas uma operação `and` ou `or` irá gerar resultado booleano.

```python
  >>> x = True
  >>> y = False
  >>> x + y
  1
  >>> x and y
  False
  >>> x or y
  True
  >>> #--------------------
  >>> x = 1
  >>> y = 0
  >>> x and y
  0
  >>> x or y
  1
  >>>       
```

## NoneType

Este é um tipo especial que serve para quando não possuimos um valor mas precisamos da variável definida pois em algum momento no decorrer do programa iremos refazer a atribuição daquela variável.

```python
>>> nome = None
>>> print(nome)
None
>>> type(nome)
<class 'NoneType'>
>>>
```

## Bytes (bytes)

Representam sequências `imutáveis` de bytes, como dados binários. Uma variável do tipo bytes pode ser usada para armazenar dados binários, como arquivos de imagem, arquivos de áudio, mensagens codificadas, entre outros. Elas são úteis quando você precisa manipular dados em nível de bytes, em vez de caracteres. Os bytes são elementos individuais de informação que `podem` representar caracteres, valores numéricos ou outros dados binários. Uma variável do tipo bytes é definida usando a notação b'', onde os bytes são especificados dentro das aspas simples ou duplas. A interpretação dos bytes depende da codificação que está sendo usada. Para converter uma sequência de bytes em uma string legível, é necessário decodificar os bytes usando uma codificação apropriada, como UTF-8, ASCII, UTF-16, entre outras.

```python
>>># Aqui temos um dados em binário utf-8
>>> dados = b'\x48\x65\x6c\x6c\x6f'
>>> print(dados)
b'Hello' # Veja a notação 
>>> print(dados[0])
72
>>> print(dados[1])
101
>>>># Vou tentar imprimir algo além da caideia de dados
>>> print(dados[100])
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: index out of range # Fora do range
>>># Decodificando os bytes e salvando na variável texto
>>> texto = dados.decode('utf-8')
>>> print(texto)
Hello
>>># Encodando para utf-16 e vendo o que acontece
>>> dados_novos = texto.encode('utf-16')
>>> print(dados_novos)
b'\xff\xfeH\x00e\x00l\x00l\x00o\x00'
>>> texto = dados_novos.decode('utf-16')
>>> print(texto)
Hello
>>># Encodando para utf-8 e vendo o que acontece
>>> dados_novos = texto.encode('utf-8')
>>> print(dados_novos)
b'Hello'
>>>
```

- **Geralmente** trabalhamos sempre com utf-8 na maioria dos casos.

Esses são tipos primitivos do Python, mas você pode criar os seus próprios como veremos mais para frente.

>A base para iniciar o restante é entender exatamente quais as variáveis do python Siga a [documentação oficial](https://docs.python.org/3/tutorial/introduction.html) e faça varios testes.

Por exemplo, você pode fazer o seguinte no Python:

```python
>>> x = 10  # x é um inteiro
>>> print(type(x))
<class 'int'>
>>> x = "hello"  # x agora é uma string
>>> print(type(x))
<class 'str'>
>>> x = 3.14  # x agora é um float
>>> print(type(x))
<class 'float'>
>>> x = True  # x agora é um bool
>>> print(type(x))
<class 'bool'>
```

Isso é diferente de linguagens estaticamente tipadas, onde você precisa declarar explicitamente o tipo de uma variável. Esse tipo de abordagem pode requer alguns cuidados.

```python
>>> x: int = 3
>>> y = 3
>>> x + y
6
>>> x = True
>>> x + y
4 # Mesmo o x sendo agora um bool onde true quer dizer 1 ele aceitou a soma e eu tinha avisado que a variável x deveria ser um inteiro.
>>> x = "um texto qualquer" 
>>> x + y
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
```

Esses são os tipos de limites que você precisa conhecer em uma liguagem. Ser uma liguagem que definem variáveis como dinamicamente tipado dá um poder maior para o programador, tanto para resolver problema ou para fazer besteira.

## Strings, ou cadeia de caracteres (srt)

Até aqui falamos de caracteres isolados como `A`, `B`, `🍉` mas ao programar também precisaremos juntar esses carecteres para formar palavras e frases, quando criamos uma variável do tipo texto em Python ele através da presença de aspas sejam elas simples `'` ou duplas `"` armazena esse valor em uma classe do tipo `str` e este tipo de dado pode armazenar um ou mais caracteres.

```py
>>> nome = "David"
>>> type(nome)
<class 'str'>
```

E como você já deve ter imaginado aqui estamos armazenando cada uma das letras `D`, `a`, `v`, `i`, `d` com seus respectivos bytes e sequencia posicional em um único objeto. (a palavra string cadeia ou corrente).

A palavra `"David"` é uma lista contendo em cada posição um caractere da tabela `utf8`.

```py
>>> list(bytes(nome, "utf-8"))
[68, 97, 118, 105, 100]
```

- 68  é o `D`
- 97  é o `a`
- 118 é o `v`
- 105 é o `i`
- 100 é o `d` minusculo

Bem, para guardar o nome "Bruno" você mais uma vez não precisa se procupar com esses detalhes todos, basta fazer `nome = "David"` e usar este texto para efetuar
as operações que você desejar.

## Exercícios

Primeiro tente fazer sozinho buscando na documentação oficial como fazer, caso não consiga, vá em [Examples/1-variaveis/](../examples/1-variaveis/) e veja o código.

Para iniciar do jeito certo, é necessário ter o `Pylint` instalado no seu VSCode. Isso ajudará e forçará no uso de boas práticas.

### Exercício 1 - Hello World

> - É boa prática todo script Python terminar com uma linha em branco.
> - É uma boa prática variáveis literais (constantes) serem declaradas como maiusculo(UPPER_CASE).
> - Também é boa prática que todo script Python explique sua existencia.
> - É uma boa prática que nomes de arquivos não use `-` mas `_`, nem números, por isso deve ser criado hello_world.py e não hello-world.py

Crie um script, [hello_world.py](../examples/hello-world.py), com uma variável chamada **MSG** contendo "Olá mundo!" e printe `o conteúdo` de MSG.

Funcionaria sem as boas práticas? Sim... Mas você quer aprender direito ou não?

E além do comentário de documentação, chamado **DocString** é também comum a inclusão de variavéis de metadados que inician e terminam com 2 underlines `__` a palavra que usamos para designar essas variavéis é `Dunder` portanto, **Dunder version** se refere a `__version__`.

### Exercício 2 - Inputs

Crie um script, [inputs.py](../examples/1-variaveis/inputs.py), pergunte o nome do usuário e mostre uma mensagem de boas vindas.

<details>
  <summary>Passe pelo lint</summary>
  No primeiro exercicio a variável é definida como UPPER_CASE (maiúscula) por que é uma variável considerada de valor constante, ou seja, final, que não irá sofrer alterações. Neste exercício a variável que irá receber o nome da pessoa deve ser lower case, ou seja minuscula. Funciona sem isso? Sim... Mas você quer aprender direito né?
</details>

### Exercício 3 - Formatando strings

Copie o inputs.py do exercício anterior para format.py e faça com que nome apareça no meio da string sem somar varias strings adicionando ao final "É um prazer ter você aqui!.

<details>
  <summary>Procure por f-string</summary>
    Tanto o format quanto o f-string resolve o nosso problema. O lint já mostrar para fazer com o f-string mas ambos os códigos abaixo funcionam.
    <pre><code class="html">
print(f"Bem vindo {nome} é um prazer ter você aqui!")
print("Bem vindo {} é um prazer ter você aqui!".format(nome))
    </code></pre>
</details>

Assim sim algo como feito abaixo funcionaria, apesar de não ser bem feito:

```python
nome = "David"
idade = "37"
email = "david@example.com"
## exemplo 1
print("O email de " + nome + " de " + idade + " anos é " +  email )
## exemplo 2
print("O email de",nome,"de",idade,"anos é",email )
```

## Sobre inputs

Quando lemos uma varíavel digitada pelo usuário usando o input, se não forçar que ela tenha um tipo, será considerado uma string. Execute o [inputs_explain](../examples/1-variaveis/inputs_explain.py) e veja o código.

```bash
Digite seu nome: David       
Digite sua idade: 37
nome tem o tipo <class 'str'>
idade tem o tipo <class 'str'>
```

### Exercício 4 - Inputs com inteiros

Baseado no código do [inputs_explain](../examples/2-exercicios/inputs_explain.py) crie o inputs_inteiro.py garantindo que a idade seja um inteiro, como na saída abaixo.

```bash
Digite seu nome: David
Digite sua idade: 37
nome tem o tipo <class 'str'>
idade tem o tipo <class 'int'> #<-----
```

Faça alguns testes digitando em idade qualquer coisa que não seja um número inteiro e veja o erro.

### Exercício 5 - Somador Inteiro

Crie o arquivo [inputs_int.py](../examples/1-variaveis/inputs_int.py.)
Leia duas entradas do tipo inteiro, some os valores e guarde em uma outra variável e imprima um texto motrando os valores de entrada e o resultado usando f-string na função print() como feito anteriormente em outros exercícios
Faça um teste digitando valores não inteiro e veja o erro.

## Objeto

Tudo no python é um `objeto`. Quando uma variável é do tipo **string** ela é um objeto de classe do tipo string.

Quando forçamos um input ser do tipo **int** ele é um objeto de classe do tipo int.

Cada classe tem seus próprios métodos de manipulação de valores. Com inteiros podemos fazer coisas que não podemos fazer com strings e vice versa.

Um objeto contém atributos e métodos que manipulam os valores dos atributos.

No caso de uma variável do tipo string, que é um objeto de classe string, temos o atributo que é o valor real da string e vários métodos (funções) que trabalham em cima desse valor.

Se você começar um script python na IDE sendo que o nome do script já tem o .py no final, a IDE irá entender que aquele já é um arquivo Python e irá te ajudar.

Ao declarar `X do tipo string`, quando apertamos o ponto (.) irá aparecer na IDE vários possíveis métodos que podemos usar com esse valor. Essa é uma das vantagens que é ter uma IDE preparada para trabalhar com uma linguagem.

Podemos querer saber os valores dessa string são númerico, alpha numéricos, se está em maisuculo, minusculo, etc. Veja o que uma IDE nos traś.

![metodos](../pics/metodos.png)

Por exemplo essa string X que declarei pode ser transformada inteiro?

Veja o script python [objetos_string](../examples/1-variaveis/objetos_string.py) com vários testes e execute.

### Exercício 6 - Objeto

Usando uma IDE para te ajudar, confira os diferentes métodos que possuímos quando declaramos uma variável de outros tipos.

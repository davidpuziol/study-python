# Apresentando

## Usando o Interpretador python

Para enviar uma instrução (ou comando) para o Python, use o `-c`

```bash
python -c "1 + 1"
```

No comando acima o Python recebe a expressão `1 + 1` interpreta, executa a operação matemática de adição, porém não exibe nenhum output, para ter a resposta na tela precisamos usar a função `print`

```bash
python -c "print(1 + 1)"
2
```

Outra maneira de se comunicar com o Python é através da execução de módulos
com o `-m`

```bash
python -m site
```

`-m nome_do_modulo` executa um módulo que esteja instalado e habilitado para execução direta, alguns exemplos `json, http.server, pip, site`

A primeia coisa que devemos entender é que quando digitamos somente o comando `python`, ele entrará no modo console e tudo escrever já será interpretado. No comando abaixo, somente fiz o nosso `Hello World` padrão.

```bash
❯ python
Python 3.11.3 (main, Apr  5 2023, 15:52:25) [GCC 12.2.1 20230201] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print('Hello DevOps')
Hello DevOps
>>> 
```

Poderíamos digitar no console qualquer coisa que o Python comp que já iria se interpretado.

```bash
❯ python 
Python 3.11.3 (main, Apr  5 2023, 15:52:25) [GCC 12.2.1 20230201] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 4+5*3/7
6.142857142857142
>>> import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
>>> 
```

Outro modo, é executar após o comando o arquivo contendo o código que será interpretado.
Confira o arquivo [hello-world.py](./examples/hello-world.py) e rode ele com o interpretador Python.

```bash
python ./examples/hello-world.py 
Hello DevOps
```

Em python é possível adicionar partes do código que serão ignoradas `(comentários no código)` pelo interpretador, essas linhas são úteis para adicionar comentários, lembretes e
metadados do programa.

Exemplos

```python
# Era uma vez um devops!

print("Hello")  # Que queria aprender python

"""
este é um bloco de comentário
multido tipo multilinhas
bom para escrever texto!
"""
```

Em ambientes Linux é muito importante definir o comentário especial `Shebang`, nele especificamos qual interpretador será usado para executar o programa.

```python
#!/usr/bin/env python3

print("Hello, World!")
```

A primeira linha informa o terminal que aquele programa roda com o Python3 da `env`
em execução, esta forma é possivel omitir o interpretador e executar o script
diretamente pelo seu nome.

```bash
# primeiro damos parmissão de execução ao script
chmod +x hello.py
```

Agora podemos executar de 2 formas

```bash
# especificando o interpretador na linha de comando
python3 hello.py
```

```bash
# usando o interpretador especificado na linha `#!/usr/bin/env python`
./hello.py
```

A vantagem da segunda forma é que podemos mudar a extensão de `.py` para `.exe`
por exemplo, ou podemos até remover a extensão e executar `./hello`

## Dunder

E além do comentário de documentação, chamado **DocString** aqueles que você escreve para explicar alguma coisa, é também comum a inclusão de variavéis de metadados que inician e terminam com 2 underlines `__` a palavra que usamos para designar essas variavéis é `Dunder` portanto,
**Dunder version** se refere a `__version__`.

## Preparando uma IDE

Somente o terminal é necessário para desenvolver em Python, mas ter um programa que nos auxilia durante a criação de código aumentará a nossa produtividade.

Existem várias IDEs (Integrated Development Environment), mas o meu preferido é o [VScode](https://code.visualstudio.com/download). A vantagem do uso é o enorme conjunto de plugins que extendem suas funcionalidade para trabalhar com diversas linguagens, ferramentas, etc.

No caso do Python instale esta extensão [Python](https://marketplace.visualstudio.com/items?itemName=ms-Python.Python).
![vscode](./pics/vscode.png)

Esta extensão instalará outra extensões complementares como a [pylance](https://marketplace.visualstudio.com/items?itemName=ms-Python.vscode-pylance) e [Jupyter](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter).

Instale também a extensão [Pylint](https://marketplace.visualstudio.com/items?itemName=ms-Python.pylint). Esta extensão te ajudará a aprender a formatar o código e usar as boas práticas de programação em Python.

As vantagems do uso de um ambiente preparado são:

- Codar mais rápido, pois possui um sistema de edição de texto mais avançado, com realce de sintaxe, preenchimento automático, formatação automática e correção.
- Depurar o código para procurar erros é mais fácil.
- Gerenciamento do projeto incluindo ferramentas que facilitam o versionamento do código como o Git.
- Integração com outras ferramentas de terceiros através do extensões.
- Terminal integrado.
- Fluxo de trabalho simplificado combinando várias ferramentas em um único lugar.
- Etc

`E tudo isso te tornará mais produtivo`

# Módulos

<https://docs.python.org/pt-br/3/tutorial/modules.html>

Módulos são conjunto novos recursos que podemos importar para o Python. O que é um plugin para um programa, uma extensão para o navegador, é o que o módulo é para no Python. Fazendo outra analogia, compramos um carro básico (Python) e colocamos várias melhorias(Módulos Python), como ar condicionado, direção hidráulica, teto solar, banco de couro, etc. Em outras linguagens também chamamos de bibliotecas, pacotes...

Não faria sentido que o interpretador Python tivesse tudo o que tem disponível se só fossemos criar um programa para ler e escrever do banco de dados. Teríamos um binário inchado de recursos não uteis para esse caso e muitos recursos que poderíam inclusive gerar falhas de segurança.

Dessa forma precisamos carregar os módulos a medida que fosse necessário.

Um módulo pode conter definições de funções que são consideradas partes executáveis ou definições de classes, como se fosse criar um novo tipo de objeto.

Pense em uma função como um adaptador de tomada. Você tem entradas e um saída. As vezes uma única entrada só para transformar algo em uma saída específica, ou tem 2, 3 entradas para também gerar uma saída específica. Às vezes tem até 10 entradas como no caso de uma régua de tomada, mas só uma saída.

Módulos são criados para serem reaproveitados novamente. Se você tem uma funcionalidade em um programa A a mesma no programa B e no C, está na hora de criar um módulo.

No fim das contas programas é igual montar lego. Pense nos módulos como vários tipos de peça que você vai deixando disponível para na sua caixinha.

Para trazer um módulo para dentro precisamos importar usando o `import`.

Uma coisa que vejo muito fazerem é importar um módulo inteiro para usar apenas 1 recurso. É como baixar um álbum completo para ouvir somente uma de 15 músicas que temos.

Vamos levar isso para o módulo `math`. O módulo math nos trás vários recursos de matemática, mas vamos usar somente a função de cálculo de raiz quadrada (sqrt), veja a diferença.

Nesse caso importei o módulo math completo para usar função sqrt() que recebe um número de entrada e devolve a raíz quadrada dele que foi atribuída a varíavel raiz. Esse é o caso ficamos com todas as funcionalidade disponíveis para uso do módulo math.

```python
>>> import math
>>> raiz = math.sqrt(49)
>>> print(raiz)
7.0
```

Nesse caso importei somente o sqrt do módulo math e observe.

```python
>>> from math import sqrt
>>> raiz = sqrt(49)
>>> print(raiz)
7.0
>>>
```

Se quisessemos dar outro nome para o módulo math poderíamos fazer o seguinte...

```python
>>> import math as matematica
>>> raiz = matematica.sqrt(49)
>>> print (raiz)
7.0
>>> 
```

## Onde eu encontro os módulos? Documentação oficial <https://docs.python.org/3/library/index.html>

Voltando ao assunto de ter uma IDE preparada, se você digitar `import` em um script python (arquivo com extensão .py) e se seguiu as recomendações com o vscode, aperte `CTRL + espaco` e veja o vscode te mostrando todos módulos built in do python.

![vscode_imports](../pics/vscode_imports.png)

De acordo com o que você vai precisando, você vai estudando. Não precisa conhecer tudo isso. Até o fim do estudo você conhecerá os principais módulos built in, e quando você fizer algo específico, estude o caso. Dá uma olhada por cima do que tem disponível só para conhecer mesmo.

>Tem módulo pra quase tudo, pode acreditar. A sua dor tem 99.99999% de chance de ser a de alguém.

Além dos módulos built in do Python, temos pacotes extras que podem ser desenvolvidos por teceiros e que você pode importar para uso.

Se você for em <https://pypi.org/> procure pelo o que possa te interessar e veja se já tem algo feito. Além disso quando você fizer algo e quiser disponibilizar para a comunidade poderá publicar aqui.

>Atenção: Sempre verifique a licença do código de terceiros e o mantenedor do módulo. Dê preferência as licenças MIT, BSD e Apache. Procure sempre por módulos que sofrem updates, possui mais de um mantenedor e tenha boas estrelinhas.

`Quando tiver conhecimento suficiente, analise o código do módulo, sempre é bom para aprender e ver o que tem dentro antes de importar pro seu código.`

Vamos fazer um teste procurando por qrcode

![qrcode](../pics/pypi_qrcode.png)
![qrcode2](../pics/pypi_qrcode_chec.png)

## Ambientes virtuais

Se instalarmos as bibliotecas do PyPI diretamente no Python principal do sistema podemos criar conflitos e deixar o ambiente muito cheio de biblitecas que podem se tornar obsoletas. Para resolver esse problemas o recomendado é criarmos um **sandbox**, um ambiente separado onde podemos ter uma cópia do ambiente Python isolada onde não corremos o risco de criar conflitos.

Vamos agora preparar um projeto com um venv (virtual environment)

```bash
# criando uma pasta para um projeto e entrando nela
mkdir projeto
cd projeto
# criando o arquivo main.py do projeto
touch main.py
# criando uma pasta venv no projeto e inicializando o virtual environment
python -m venv venv

# Se quiser a pasta oculta
# python -m venv .venv
```

Observe que ela criou uma copia do python para dentro dessa pasta
```bash
❯ ls -a venv 
total 24K
drwxr-xr-x 5 david david 4.0K Jun  3 11:43 .
drwxr-xr-x 3 david david 4.0K Jun  3 11:43 ..
drwxr-xr-x 2 david david 4.0K Jun  3 11:43 bin
drwxr-xr-x 3 david david 4.0K Jun  3 11:43 include
drwxr-xr-x 3 david david 4.0K Jun  3 11:43 lib
lrwxrwxrwx 1 david david    3 Jun  3 11:43 lib64 -> lib
-rw-r--r-- 1 david david  169 Jun  3 11:43 pyvenv.cfg
```

Ali dentro da pasta `bin` é onde encontramos o `python` e também outras ferramentas como o `pip` e a partir de agora todos os módulos que instalarmos vão para dentro da pasta `lib`deste projeto.

Voce já tem um projeto um virtual environment, mas é necessário ativar esse ambiente quando for usar e desativar quando não for. Dentro da pasta venv/bin/ temos o activate que precisa ser carregado.

```bash
source ./venv/bin/activate
```

Para se certificar que o ambiente esta rodando corretamente use o modulo site. Observe que algumas pastas mudaram de para o path do projeto.

```bash
❯ python3 -m site
sys.path = [
    '/home/david/gitlab/projeto',
    '/usr/lib/python311.zip',
    '/usr/lib/python3.11',
    '/usr/lib/python3.11/lib-dynload',
    '/home/david/gitlab/projeto/venv/lib/python3.11/site-packages',
]
USER_BASE: '/home/david/.local' (exists)
USER_SITE: '/home/david/.local/lib/python3.11/site-packages' (doesn't exist)
ENABLE_USER_SITE: False
```

E também podemos conferir qual o binário python será acionado quando invocado

```bash
which python
/home/david/gitlab/projeto/venv/bin/python
```

Para desativar somente digite

```bash
deactivate

# para conferir execute o site novamente e veja para onde esta apontando
❯ python3 -m site
sys.path = [
    '/home/david/gitlab/projeto',
    '/usr/lib/python311.zip',
    '/usr/lib/python3.11',
    '/usr/lib/python3.11/lib-dynload',
    '/usr/lib/python3.11/site-packages',
]
USER_BASE: '/home/david/.local' (exists)
USER_SITE: '/home/david/.local/lib/python3.11/site-packages' (doesn't exist)'
ENABLE_USER_SITE: True

#Confira agora qual o python que esta carregado
❯ which python
/usr/bin/python
```

>Sempre que abrir um terminal, antes de executar os comandos você deverá ativar o ambiente virtual do seu projeto.

No vscode se na pasta do projeto ele encontrar o venv irá mostrar junto à extensão.

>Se você estiver versionando o código lembre de por a pasta venv ou .venv para não versionar senão seu repositório irá ficar muito grande, pois essa pasta tem centenas de arquivos.

```bash
echo 'venv/' >> .gitignore
```

## Instalando módulos

Certifique-se de estar em algum ambiente virtual primeiro, senão será instalado no sistema inteiro.

<https://packaging.python.org/en/latest/tutorials/installing-packages/>

Na própria documentação do módulo mostra o comando para instalá-lo usando o `pip`. O pip é o gerenciador de pacotes do Python. O pip irá buscar no pypi pelo módulo para poder instalar no seu sistema. Porém se fizer isso direto no sistema esse módulo ficará disponível em todo o seu sistema operacional e fora da pasta do projeto.

Para instalar o gerenciador pip:

```bash
## base debian
sudo apt install python-pip
## base red-hat
sudo yum install python-pip
## base arch
sudo pacman -S python-pip
## Windows no cmd
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
```

Para atualize o pip usando o pip!

```bash
python3 -m pip install --upgrade pip
```

Vamos instalar por exemplo um pacote que é um outro terminal python (`IPython`) mais evoluido do que o terminal padrão. Esse terminal tem saídas coloridas para o output, facilita a leitura e tem ferramentas para mais fáceis para obter ajuda. O Ipython é uma versão do interpretadotr Python que possui mais funcionalidades e tem o tab para o auto-complete!

```bash
python3 -m pip install ipython
```

```bash
❯ ipython
Python 3.11.3 (main, Apr  5 2023, 15:52:25) [GCC 12.2.1 20230201]
Type 'copyright', 'credits' or 'license' for more information
IPython 8.14.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: 6+5
Out[1]: 11

In [2]: ?

In [3]: p<tab>
 pass       property   %pastebin  %pdoc      %pinfo     %popd      %prun      %psource   %pycat     %%python  
 pow()      %page      %pdb       %%perl     %pinfo2    %pprint    %%prun     %pushd     %pylab     %%python2 
 print()    %paste     %pdef      %pfile     %pip       %precision %psearch   %pwd       %%pypy     %%python3 
```

## Exercicio 1 - random

Crie um arquivo random.py e importe o módulo random e imprima um numero aleatório criado por essa biblioteca.

---

Crie um projeto com um venv e ative-a par os próximos exercícios. todos deverão estar no mesmo projeto.

---

## Exercicio 2 - emojis

Instale uma lib de emojis e print alguns na tela, faça exemplos que a própria lib te disponibilizou. O código esta em ![emojis.py](../examples/3-modulos/Projetos/exercicios/emojis.py)

## Exercicio 3 - Int

Leia um número real e somente mostre a parte inteira. Utilize módulo math para isso.
O código esta em [int_part.py](../examples/3-modulos/int_part.py)

>Também existe a conversão por tipo de variáveis, não vamos esquecer disso, mas não use nesse exercício.

```python
>>> real = float(4.5677)
>>> inteiro = int(real)
>>> print(inteiro)
4
>>> 
  ```

## Exercicio 4 - Sorteio

Leia o nome de 4 frutas e sorteia uma. Para esse exercício somente importe exatamente o que irá usar. Código em [sort_frutas.py](../examples/3-modulos/sort_frutas.py)

## Exercicio 5 - Sorteio com Ordem

Leia o nome de 4 frutas e sorteia uma. Para esse exercício somente importe exatamente o que irá usar. Código em [sort_frutas.py](../examples/3-modulos/sort_frutas.py)

## Exercicio 6 - Tocar música (Desafio)

Utilize um módulo python para abrir e reproduzir um arquivo mp3. Tente procurar módulos que façam este trabalho, não reinvente a roda. Só veja o código se realmente não conseguiu fazer. O arquivo mp3 pode ser da sua escolha ou o [Euphoria.mp3](../examples/3-modulos/Projetos/exercicios/Euphoria.mp3.). Coloque o arquivo junto com o script para facilitar sua vida nesse moemnto. `É necessário que você busque o máximo que conseguir na Internet como fazer isso, para mais tarde falarmos do seu sofrimento`. Só veja o código depois que ter sofrido muito... Código em [playmp3](../examples/3-modulos/Projetos/exercicios/playmp3.py).

<details>
  <summary>Dica </summary>
    Procure por pygame para importar <a href="ttps://www.pygame.org/docs/ref/mixer.html">pygame-mixer</a>. Lembre-se que existem muitos módulos disponíveis, não é somente este que resolve o problema. Instale a lib no projeto, revise o venv se necessário.
    Se importar a lib do python chamada <a href="https://docs.python.org/3/library/os.html">os</a>, somente com o código abaixo conseguiria tocar se tivesse algum audio player instalado na sua máquina definido para tocar mp3, mas use uma lib como exercício.

```python
import os
os.system("mpg123 " + "Euphoria.mp3")
```

</details>

## Vamos ter uma conversinha

O mundo de TI é um estudo eterno. Às vezes você agarra em uma linha de código e passa o dia para achar o problema, ou como resolver. É um bom momento para refletir se isso é pra você ou não. Programar é um desafio constante.

## Escolha corretamente os módulos

Muitos módulos param de ser atualizados com o passar do tempo. Desenvolvedores abandonam seus projetos fácilmente, pois já não usam mais, ou estão envolvidos com outra coisa, etc. Falhas de segurança, bugs, e outras coisas podem aparecer.

CVE significa "Common Vulnerabilities and Exposures" (Vulnerabilidades e Exposições Comuns). É um sistema internacional de identificação, classificação e catalogação de vulnerabilidades de segurança em software e hardware. É um banco de dados que mostram as vulnerabilidades das libs e se foram corrigidas em alguma outra versão ou não.

- Se uma lib não tem ninguém corrigindo essas falhas ficará com a vulnerabilidade exposta. Eis a importância de usar módulos bem conceituados e com bastante mantenedores, boa avaliação, para evitar que VC, tenha que resolver o problema.
- Libs que sofrem atualização constante além de corrigir as falhas trazem novas funcionalidades.
- Algumas libs somente funcionam com versão específica do python por não tem atualização para as novas versões, forçando o uso de um python antigo que também podem ter vulnerabilidades.

>Procure na internet as libs mais usadas verifique no <pypi.org> antes de usar.

## Módulo que precisa ser dominado

Não precisa se apavorar, mas até terminar o estudo tente dominar estas libs:

- RE: que faz tratamento de regex, para validação de dados
- DateTime: Para manipular time
- Statistics: Para completar a lib math
- functools: Funções que recebem e retornam outra função.
- Socket: para se comunicar atraves da rede

## Módulos para ter em mente no futuro

Aqui alguma bibliotecas para ficar no conhecimento que existem e podem ser utilizadas no ambiente de trabalho no futuro.
Não estão listadas aqui bibliotecas que são nativas do python, somente aquelas que precisam ser importadas.

>Não se importe em dominá-las enquanto você não precisar, vá ganhando conhecimento ao longo do caminho. Saiba que existe para quando você precisar.

Para sites:

- <https://pypi.org/project/requests/>
- <https://pypi.org/project/Flask/>
- <https://pypi.org/project/Django/>

Para data science e inteligencia artificial:

- <https://pypi.org/project/numpy/>
- <https://pypi.org/project/pandas/>

Gráfico de visualização dos dados:

- <https://pypi.org/project/plotly/>
- <https://pypi.org/project/matplotlib/>
- <https://pypi.org/project/seaborn/>
- <https://pypi.org/project/streamlit/>

Inteligencia artificial

- <https://pypi.org/project/tensorflow/>
- <https://pypi.org/project/keras/>
- <https://docs.opencv.org/>
- <https://scikit-learn.org/stable/>
- <https://github.com/pytorch/pytorch>
- <https://www.nltk.org/>
- <https://pypi.org/project/Pillow/>

Automacoes:

- <https://pypi.org/project/selenium/>
- <https://pypi.org/project/Scrapy/>
- <https://www.crummy.com/software/BeautifulSoup/bs4/doc/>
- <https://pypi.org/project/PyAutoGUI/>
- <https://pypi.org/project/pyodbc/>
- <https://pypi.org/project/pywin32/>

Criancao de interface gráficas:

- <https://kivy.org/doc/stable/>
- <https://pypi.org/project/pygame/>

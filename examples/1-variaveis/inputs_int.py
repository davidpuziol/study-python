#!/usr/bin/env python3

"""
Somador inteiro
"""
valor1 = int(input("Digite o primeiro valor: "))
valor2 = int(input("Digite o segundo valor: "))
resultado = valor1 + valor2
print(f"A some dos valores {valor1} e {valor2} = {resultado}")

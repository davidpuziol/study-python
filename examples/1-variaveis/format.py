#!/usr/bin/env python3

"""
Aprendendo f-string
"""
nome = input("Digite seu nome: ")
print(f"Bem vindo {nome} é um prazer ter você aqui!")

#print("Bem vindo {} é um prazer ter você aqui!".format(nome))

#!/usr/bin/env python3

"""
Forçando input inteiro
"""
nome = input("Digite seu nome: ")
idade = int(input("Digite sua idade: "))
print(f"nome tem o tipo {type(nome)}")
print(f"idade tem o tipo {type(idade)}")

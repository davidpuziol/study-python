#!/usr/bin/env python3

"""
Entendendo objetos
"""

X = "string simples"
print(f"X vale {X}")

print(f"X é somente um número? {X.isnumeric()}")
print(f"X tem somente números e letras? {X.isalnum()}")
print(f"X esta todo em maiúsculo? {X.isupper()}")
print(f"X esta todo em minúsculo? {X.islower()}")
print(f"X termina com interrogação ? {X.endswith('?')}")
print(f"X encodado em utf-16 é {X.encode('utf-16')}")
print(f"X é decimal ? {X.isdecimal()}")

X = "STRING SIMPLES ?"
print(f"X vale {X}")

print(f"X é somente um número? {X.isnumeric()}")
print(f"X tem somente números e letras? {X.isalnum()}")
print(f"X esta todo em maiúsculo? {X.isupper()}")
print(f"X esta todo em minúsculo? {X.islower()}")
print(f"X termina com interrogação ? {X.endswith('?')}")
print(f"X encodado em utf-16 é {X.encode('utf-16')}")
print(f"X é decimal ? {X.isdecimal()}")

X = "39"
print(f"X vale {X}")

print(f"X é somente um número? {X.isnumeric()}")
print(f"X tem somente números e letras? {X.isalnum()}")
print(f"X esta todo em maiúsculo? {X.isupper()}")
print(f"X esta todo em minúsculo? {X.islower()}")
print(f"X termina com interrogação ? {X.endswith('?')}")
print(f"X encodado em utf-16 é {X.encode('utf-16')}")
print(f"X é decimal ? {X.isdecimal()}")

X = "34.5"
print(f"X vale {X}")

print(f"X é somente um número? {X.isnumeric()}")
print(f"X tem somente números e letras? {X.isalnum()}")
print(f"X esta todo em maiúsculo? {X.isupper()}")
print(f"X esta todo em minúsculo? {X.islower()}")
print(f"X termina com interrogação ? {X.endswith('?')}")
print(f"X encodado em utf-16 é {X.encode('utf-16')}")
print(f"X é decimal ? {X.isdecimal()}")

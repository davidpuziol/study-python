#!/usr/bin/env python3

"""
Exercicio 9 = soma div3 e impar
"""

final = int(input("Quer somar de zero ate qual numero: " ))
soma = 0
contador = 0

#Essa logica funciona, mas o desempenho é pior
#Começamos no 1 que é o primeiro impar e pulamos de 2 em 2
# for n in range(1,final+1,2):
#     if n % 3 == 0:
#         soma = soma + n
#         contador += 1

# O primeiro divisivel por 3 é o proprio 3 começa por ele.
# O proximo multiplio de 3 é 6, mas é par, e o outro é 9.
# Temos um padrão de que somar 3 achamos o proximo mais é par, mas se somar 6
# Sempre teremos um proximo impar.
for n in range(3,final+1,6):
    soma = soma + n
    contador += 1
    
print(f"A soma é {soma}")

print(f"Contador é {contador}")

print(f"A lista tem {len(range(0,final+1,6))}")

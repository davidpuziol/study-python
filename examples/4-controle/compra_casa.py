#!/usr/bin/env python3

"""
Exercicio 2 = Compra casa
"""

valor = float(input("Qual o valor da casa? "))
renda = float(input("Qual a renda mensal do comprador? "))
anos  = int(input("Em quantos anos deseja pagar? "))

parcela = valor/(anos*12)

print(f"A parcela ficará em {parcela}")

if parcela <= renda*30/100:
    print(f"A parcela ficará em {parcela}")
else:
    print(f"Com a renda de {renda} o comprador não consegue comprar "
        f"a casa de {valor} pois a parcela de {parcela} "
        f"excede 30% do salário {renda*30/100}")

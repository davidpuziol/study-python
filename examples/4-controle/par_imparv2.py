#!/usr/bin/env python3

"""
Exercicio 8 = Par impar
"""

inicial = int(input("Numero inicial: " ))
final = int(input("Numero final: " ))

#Init será o nosso valor base
init = inicial

# Se for divisivel por 2 então vamos iniciar o init com o próprio numero
# senão vamos para o próximo.
if (inicial % 2) == 0:
    init = inicial
else:
    init = inicial +1

# Se o primeiro que vamos criar a lista é par a partir do init,
# todos que pularem de 2 em 2 é par também
for sec in range(init,final+1,2):
    print(sec)

# Se não for divisivel por 2 então é impar e reiniciamos o init, 
# caso contrário é par e vamos pro próximo.

if (inicial % 2) != 0:
    init = inicial
else:
    init = inicial+1
# Se o primeiro que vamos criar a lista é impar a partir do init,
# todos que pularem de 2 em 2 é impar também.
print("Numeros impares")
for sec in range(init,final+1,2):
    print(sec)

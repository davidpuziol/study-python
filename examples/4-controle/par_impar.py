#!/usr/bin/env python3

"""
Exercicio 8 = Par impar
"""
inicial = int(input("Numero inicial: " ))
final = int(input("Numero final: " ))

print("Numeros pares")
for sec in range(inicial,final+1):
    if (sec % 2) == 0:
        print(sec)

print("Numeros impares")
for sec in range(inicial,final+1):
    if (sec %2) != 0:
        print(sec)

#!/usr/bin/env python3

"""
Exercicio 6 = Jokenpo
"""

from random import randint

jokenpo = ["Pedra", "Papel", "Tesoura"]
while True:
    print('''Digite:
    [0]Pedra
    [1]Papel
    [2]Tesoura
    [3]Sair''')
    entrada = int(input("Opcao: "))
    if entrada >= 3:
        break
    else:
        computador = randint(0, 2)
        print(f"Computador escolheu {jokenpo[computador]}")
        print(f"Jogador    escolheu {jokenpo[entrada]}")
        if computador == entrada: # Jogadas iguais
            print("Empate")
        else:
            if (computador == 0 and entrada == 1) or \
               (computador == 1 and entrada == 2) or \
               (computador == 2 and entrada == 0):
                print("Jogador Ganhou")
            else:
                print("Jogador Perdeu")
    print("###############################")

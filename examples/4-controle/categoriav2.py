#!/usr/bin/env python3

"""
Exercicio 5 = Categoria Versao2
"""

from datetime import datetime
from dateutil.relativedelta import relativedelta

data_nascimento = input("Digite a data de nascimento (dd/mm/yyyy): ")

# Converter a string da data de nascimento em um objeto datetime
data_formatada = datetime.strptime(data_nascimento, "%d/%m/%Y")

# Obter a data atual
data_atual = datetime.now()

# Calcular a diferença entre as datas usando relativedelta
idade = relativedelta(data_atual, data_formatada).years

# Exibir a idade atual
print(f"A idade atual é {idade} anos.")

print(f"A idade do aluno é {idade}")

if idade < 9:
    print("Mirim")
elif idade <= 9:
    print("Infantil")
elif idade <=14:
    print("Categoria Infantil")
elif idade <=19:
    print("Categoria Junior")
elif idade <=25:
    print("Categoria Senior")
else:
    print("Categoria Master")

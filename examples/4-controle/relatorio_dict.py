#!/usr/bin/env python3
"""Exibe relatório de crianças por atividade.

Exericio 11
Imprimir a lista de crianças agrupadas por sala
que frequentas cada uma das atividades.
"""

# Dados

sala = {
    "1": ["Erik", "Maia", "Gustavo", "Manuel", "Sofia", "Joana"],
    "2": ["Joao", "Antonio", "Carlos", "Maria", "Isolda"]
}

atividades= {
    "Inglês": ["Erik","Maia","Joana", "Carlos", "Antonio"],
    "Música": ["Erik", "Carlos", "Maria"],
    "Dança": ["Gustavo", "Sofia", "Joana", "Antonio"]
}

# Listar alunos em cada atividade por sala
# usando o desempacotamento para atividade

for key, value in atividades.items():

    print(f"Alunos da atividade {key}\n")
    print("-" * 40)

    atividade_sala1 = set(sala["1"]) & set(value)
    atividade_sala2 = set(sala["2"]).intersection(value)

    print("Sala1 ", atividade_sala1)
    print("Sala2 ", atividade_sala2)

    print()
    print("#" * 40)

#!/usr/bin/env python3

"""
Exercicio 5 = Categoria
"""

from datetime import datetime

# Ira digitar a data no formato proposto
data_nascimento = input("Qual a data de nascimento (dd/mm/yyyy)? ")

# Ira digitar a data no formato proposto e converte-la para um objeto do tipo datatime
data_formatada = datetime.strptime(data_nascimento, "%d/%m/%Y")
data_atual = datetime.now()
idade = data_atual.year - data_formatada.year

if (data_atual.month, data_atual.day) < (data_formatada.month, data_formatada.day):
    idade -= 1

print(f"A idade do aluno é {idade}")

if idade < 9:
    print("Mirim")
elif idade <= 9:
    print("Infantil")
elif idade <=14:
    print("Categoria Infantil")
elif idade <=19:
    print("Categoria Junior")
elif idade <=25:
    print("Categoria Senior")
else:
    print("Categoria Master")

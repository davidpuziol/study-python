#!/usr/bin/env python3

"""
Exercicio 10  Primos
"""

numero = int(input("Digite um numero: "))
cont = 0

for n in range(2,numero):
    if numero % n == 0:
        cont += 1
if cont == 0:
    print(f"O numero {numero} é primo")
else:
    print(f"O numero {numero} NAO é primo")
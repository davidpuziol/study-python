#!/usr/bin/env python3

"""
Exercicio 3 = Converte numero
"""

num = int(input("Entre com um número inteiro? "))
opcao = input("Digite:\n \
1 para converter em binário\n \
2 para converter em octal\n \
3 para converter em hexa? ")

if opcao == "1":
    print(f"Em binario {bin(num)[2:]}")
elif opcao == "2":
    print(f"Em octal {oct(num)[2:]}")
else:
    print(f"Em hexa {hex(num)[2:]}")

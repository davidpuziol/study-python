#!/usr/bin/env python3

"""
Exericio 1 - numero randomico
"""
import random

numero_aleatorio = random.random()
print(f"Um número aleatório entre de 0 e 1 é {numero_aleatorio}")

numero_aleatorio = random.uniform(1,10)
print(f"Um número aleatorio entre 1 a 10 é {numero_aleatorio}")

numero_aleatorio = random.randint(1,100)
print(f"Um número aleatório inteiro entre de 1 a 100 é {numero_aleatorio}")

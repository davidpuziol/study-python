#!/usr/bin/env python3

"""
Exericio 6 - Play mp3
"""

import pygame

pygame.mixer.init()
sound = pygame.mixer.music.load("Euphoria.mp3")
pygame.mixer.music.set_volume(0.7)
pygame.mixer.music.play()

#Se não esperar ele vai mandar tocar e o programa já vai morrer.
pygame.time.wait(25000) # = 25 segundos

##########
# import os
# os.system("mpg123 " + Euphoria.mp3)
##########

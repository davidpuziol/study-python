#!/usr/bin/env python3

"""
Exericio 2 - emojis
"""

import emoji

print(emoji.emojize('Python is :thumbs_up:'))
print(emoji.emojize('Python is :thumbsup:', language='alias'))
print(emoji.demojize('Python is 👍'))
print(emoji.emojize("Python is fun :red_heart:"))
print(emoji.emojize("Python is fun :red_heart:", variant="emoji_type"))
print(emoji.is_emoji("👍"))


print(emoji.emojize('Python es :pulgar_hacia_arriba:', language='es'))
print(emoji.demojize('Python es 👍', language='es'))
print(emoji.emojize("Python é :polegar_para_cima:", language='pt'))
print(emoji.demojize("Python é 👍", language='pt'))

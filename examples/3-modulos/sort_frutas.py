#!/usr/bin/env python3

"""
Exercicio 4 - Sorteando fruta
"""

from random import choice

fruta1 = input("Digite uma fruta: ")
fruta2 = input("Digite uma fruta: ")
fruta3 = input("Digite uma fruta: ")
fruta4 = input("Digite uma fruta: ")

lista_fruta = [fruta1, fruta2, fruta3, fruta4]

print(f"A fruta sorteada é {choice(lista_fruta)}")

#!/usr/bin/env python3

"""
Exercicio 5 - Sorteando fruta
"""

import random

fruta1 = input("Digite uma fruta: ")
fruta2 = input("Digite uma fruta: ")
fruta3 = input("Digite uma fruta: ")
fruta4 = input("Digite uma fruta: ")

lista_fruta = [fruta1, fruta2, fruta3, fruta4]
random.shuffle(lista_fruta)

print(f"O embaralhamento ficou {lista_fruta}")
print(f"vai comer primeiro = {lista_fruta[0]}")
print(f"vai comer depois = {lista_fruta[1]}")
print(f"vai comer logo depois = {lista_fruta[2]}")
print(f"vai comer finalmente = {lista_fruta[3]}")

#!/usr/bin/env python3

"""
Exericio 3 - Parte inteira
"""

import math

num = float(input("Digite um número real? "))

print(f"A parte inteira  é {math.floor(num)}")

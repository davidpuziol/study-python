#!/usr/bin/env python3

"""
Importando todo os módulos
"""
import pkgutil
import importlib

# Obtém todos os módulos disponíveis
modules = pkgutil.iter_modules()

for module in modules:
    try:
        importlib.import_module(module.name)
    except Exception as e:
        print(f"Erro ao importar: {module.name}")
        print(f"Detalhes do erro: {str(e)}")
    else:
        print(f"Importado: {module.name}")

#!/usr/bin/env python3
"""Teste de modulo built in OS.

A idéia é tentar usar um pouco o módulo `os` do python e fazer comandos 
iteragir com o sistema operacional. 
Esta é uma leve introdução para que no futuro
comece a fazer scripts python ao invés de shell!


Execução:

    python3 os.py
    ou
    ./os.py
"""

__version__ = "0.0.1"
__author__ = "David Prata"
__license__ = "Unlicense"
__documentation__  = "https://docs.python.org/3/library/os.html"

import os #Não é necessário, mas ser explicito não faz mal
import time

from datetime import datetime

print(dir(os))

mydir = os.getcwd()

print("#"*20)
print(f"O meu path é {os.getenv('PATH')}")

#outra maneira
print(f"Minha variavel de ambiente HOME é {os.environ.get('HOME')}")

print("#"*20)
print(f"Mudando para o diretório home")
os.chdir("/home/david")
print(f"O meu diretório corrente é {os.getcwd()}")
os.chdir(mydir)

print("#"*20)
print(f"Os diretórios do meu home são")
print(os.listdir())

# print("#"*20)
# print(f"Criando um diretório test")
# os.mkdir("teste")


print("#"*20)
print(f"Vamos criar uma lista de diretórios passando todo o path que queremos")
os.makedirs('dir1/subdir1/subsubdir1')

time.sleep(5)

print("#"*20)
print(f"Vamos remover uma lista de diretórios passando todo o path que queremos")
os.removedirs('dir1/subdir1/subsubdir1')

print("#"*20)
print(f"Pegando informações sobre o arquivo.txt")
print(os.stat("arquivo.txt"))
print(f"O tamanho do arquivo em bytes é {os.stat('arquivo.txt').st_size}")
modtime = os.stat('arquivo.txt').st_mtime
print(f"A ultima modificação do arquivo foi {datetime.fromtimestamp(modtime)}")

# print("#"*20)
# print(f"Removendo o diretório test")
# os.rmdir("teste")

#Como o a arvore de diretórios


diretorio_pai = os.path.dirname(os.getcwd())

# for dirpath, dirnames, filenames in os.walk(diretorio_pai):
#     print('Diretorio Corrent:', dirpath)
#     print('Diretorios:', dirnames)
#     print('Arquivos::', filenames)
#     print()

# Como juntar dois paths de forma correta. Também é possível cocatenar,
# Mas geralmente esquecemos o / ou acaba ficando com //
file_path = os.path.join(os.getcwd(), 'teste.txt')
print(f"Jutando o diretorio fica {file_path}")

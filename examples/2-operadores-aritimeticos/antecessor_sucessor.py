#!/usr/bin/env python3

"""
Exercicio 1 antecessor sucessor
"""

x = int(input("Digite um número "))

antecessor = x - 1
sucessor = x + 1

print(f"O antecessor do número {x} é {antecessor} e o sucessor é {sucessor}")

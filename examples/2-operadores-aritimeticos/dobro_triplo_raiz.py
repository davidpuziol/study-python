#!/usr/bin/env python3

"""
Exercicio 2
"""

x = int(input("Digite um número "))

dobro = 2 * x
triplo = 3 * x
raiz = x**1/2

print(f"O dobro de {x} é {dobro} o triplo é {triplo} e a raiz quadrada é {raiz}")

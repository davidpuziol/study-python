#!/usr/bin/env python3

"""
Exercicio 6 tinta
"""

largura = float(input("Qual a largura da parede? "))
altura = float(input("Qual a largura da parede? "))

RENDIMENTO = 2

area = largura * altura
litros = area / RENDIMENTO

print(f"Para pintar essa parede você precisará de {litros} litros de tinta")

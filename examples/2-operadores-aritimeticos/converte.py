#!/usr/bin/env python3

"""
Exercicio 4 converte metros em centimetros e milimitros
"""

valor = float(input("Quantos metros? "))
centimetros = valor * 100
milimetros = valor * 1000

print(f"Em {valor} metros temos {centimetros} centimetros e {milimetros} milimetros")

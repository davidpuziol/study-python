#!/usr/bin/env python3

"""
Exercicio 3 média
"""

nota1 = int(input("Digite a primeira nota "))
nota2 = int(input("Digite a segunda nota "))

media = (nota1 + nota2)/2

print(f"A média do aluno é {media}")

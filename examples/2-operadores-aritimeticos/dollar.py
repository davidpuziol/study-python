#!/usr/bin/env python3

"""
Exercicio 6 dollar
"""

cotacao = float(input("Quanto custa um dollar? "))
montante_real = float(input("Quantos reais você deseja cambiar ?"))

montante_dollar = montante_real / cotacao

print(f"Você consegue comprar {montante_dollar:.2f} com {montante_real:.2f} reais")

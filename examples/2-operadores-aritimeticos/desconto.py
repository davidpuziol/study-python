#!/usr/bin/env python3

"""
Exercicio 8 - desconto
"""

valor = float(input("Qual o valor do produto? "))
DESCONTO = 8/100 * valor
valor_desconto = valor - DESCONTO

print(f"O valor com 8% de desconto é {valor_desconto:.2f} e o desconto foi de {DESCONTO:.2f}")

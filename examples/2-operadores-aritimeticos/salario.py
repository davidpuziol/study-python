#!/usr/bin/env python3

"""
Exercicio 9 - salario
"""

valor = float(input("Qual o valor do salário? "))
AUMENTO = 17/100 * valor
salario_aumentado = valor + AUMENTO

print(f"O novo salário seria de R${salario_aumentado:.2f}")

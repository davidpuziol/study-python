#!/usr/bin/env python3
"""
Exercicio 4 - Sorteando frutas
"""
from random import choice

fruta1 = input("Digite a primeira fruta: ")
fruta2 = input("Digite a segunda fruta: ")
fruta3 = input("Digite a terceira fruta: ")
fruta4 = input("Digite a quarta fruta: ")

lista = [fruta1, fruta2, fruta3, fruta4]
print(lista)

print(f"Você vai comer a fruta {choice(lista)}")
